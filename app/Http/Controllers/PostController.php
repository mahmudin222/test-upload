<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

use DB;
use App\Quotation;

class PostController extends Controller
{
    public function create(){
    	$categories = Category::All();
    	return view('post.create',compact('categories'));
    }

    public function reads(){
        $Posts = DB::table('posts')->select('posts.id as post_id','posts.category_id','posts.title','posts.slug','posts.content','categories.name')->join('categories','categories.id','=','posts.category_id')->orderBy('posts.id','desc')->get();        
        return view('post.reads',compact('Posts'));
    }

    public function store(){
    	Post::create([
    		'category_id'=>request('category_id'),
    		'title'=>request('title'),
    		'slug'=>str_slug(request('title')),
    		'content'=>request('title')
    	]);

    	return redirect(route('post.reads'))->with('success','Data Berhasil Ditambahkan');
    }

    public function edit(Post $post){
        $categories = Category::All();
        return view('post.edit',compact('categories','post'));
    }

    public function update(Post $post){

        $post->update([
            'category_id'=>request('category_id'),
            'title'=>request('title'),
            'slug'=>str_slug(request('title')),
            'content'=>request('title')
        ]);

        return redirect(route('post.reads'))->with('update','Data Berhasil Diubah');
    }

    public function delete(Post $post){

        $post->delete();

        return redirect(route('post.reads'))->with('delete','Data Berhasil DiHapus');

    }

}
