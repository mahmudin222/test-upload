<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8 col-md-offset-2">
        	@if(session('success'))
				<div class="alert alert-info">
					{{ session('success') }}
				</div>
			@endif
			@if(session('update'))
				<div class="alert alert-success">
					{{ session('update') }}
				</div>
			@endif
			@if(session('delete'))
				<div class="alert alert-danger">
					{{ session('delete') }}
				</div>
			@endif
		</div>

	</div>
</div>