@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8 col-md-offset-2">
            @foreach($Posts as $vPosts)
                <div class="card">
                    <div class="card-header">
                        <form method="post" action="{{ route('post.delete',$vPosts->post_id) }}">

                            {{ $vPosts->title }}

                            <a href="{{ route('post.edit',$vPosts->post_id) }}" class="btn btn-xs btn-info">
                                Edit
                            </a>

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button class="btn btn-xs btn-danger" type="submit">
                                Delete
                            </button>

                        </form>

                    </div>

                    <div class="card-body">
                       {{ $vPosts->content }}
                       <p>
                           Categories : {{ $vPosts->name }}
                       </p>
                    </div>
                </div>
                <br>
            @endforeach
        </div>

    </div>
</div>
@endsection