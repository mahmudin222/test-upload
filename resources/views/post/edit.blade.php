@extends('layouts.app')

@section('content')

	<div class="container">
		<form method="post" enctype="multipart/form-data" action="{{ route('post.update',$post->id) }}">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="form-group">
				<label>
					Categories
				</label>
				<select class="form-control" required name="category_id">
					@foreach($categories as $vcategories)
					<option value="{{ $vcategories->id }}"
						@if($post->category_id == $vcategories->id) selected @endif>
						{{ $vcategories->name }}
					</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>
					Post Title
				</label>
				<input type="text" class="form-control" name="title" required placeholder="Post Title"
				value="{{ $post->title }}">
			</div>
			<div class="form-group">
				<label>
					Content
				</label>
				<textarea name="content" class="form-control" required placeholder="Content">{{ $post->content }}</textarea>
			</div>
			<button type="submit" class="btn btn-info">
				Simpan
			</button>
		</form>
	</div>

@endsection